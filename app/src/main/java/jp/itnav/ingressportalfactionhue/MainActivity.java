package jp.itnav.ingressportalfactionhue;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseHueActivity implements View.OnClickListener, Response.Listener, Response.ErrorListener {

    private static final String TAG = "MainActivity";
    private static final int FACTION_CODE_NEUTRALIZE    = 0;
    private static final int FACTION_CODE_RESISTANCE    = 1;
    private static final int FACTION_CODE_ENLIGHTENED   = 2;

    private static final int NEUTRALIZE_STATE_RED_BRIGHT    = 10;
    private static final int NEUTRALIZE_STATE_WHITE_BRIGHT  = 11;
    private static final int NEUTRALIZE_STATE_RED_DARK      = 12;
    private static final int NEUTRALIZE_STATE_WHITE_DEFAULT = 13;

    private EditText formPortalLevel;
    private int currentLightIndex = 0;
    private PHHueSDK phHueSDK;
    private Timer lightTimer;

    private int   currentFaction     = 0;
    private int   currentPortalLevel = 1;
    private int[] resistanceColor    = { 0, 0, 255 };
    private int[] enlightenedColor   = { 0, 255, 0 };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        phHueSDK = PHHueSDK.create();
        setViews();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.button_light_update:
                requestServer();
                break;
            case R.id.button_faction_resistance:
                updateFaction(FACTION_CODE_RESISTANCE);
                updateLight();
                break;
            case R.id.button_faction_enlightened:
                updateFaction(FACTION_CODE_ENLIGHTENED);
                updateLight();
                break;
            case R.id.button_white_initialize:
                portalNeutralize();
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("PIA" + "onErrorResponse", String.valueOf(error));
    }

    @Override
    public void onResponse(Object response) {
        Log.d("PIA" + "onResponse", response.toString());
    }

    public PHBridge getSelectedBridge() {
        return phHueSDK.getSelectedBridge();
    }

    private void initializeLightTimer() {
        if (lightTimer != null) {
            lightTimer.cancel();
        }
        lightTimer = new Timer(true);
    }

    private void setViews() {
        formPortalLevel = (EditText) findViewById(R.id.editText_level);
        Button update = (Button) findViewById(R.id.button_light_update);
        Button res = (Button) findViewById(R.id.button_faction_resistance);
        Button enl = (Button) findViewById(R.id.button_faction_enlightened);
        Button white = (Button) findViewById(R.id.button_white_initialize);
        update.setOnClickListener(this);
        res.setOnClickListener(this);
        enl.setOnClickListener(this);
        white.setOnClickListener(this);
    }

    private int stringToInt(String param) {
        return Integer.parseInt(param);
    }

    private int getCurrentFactionColor(int index) {
        if (getCurrentPortalLevel() == 0) {
            return 255;
        }

        if (currentFaction == FACTION_CODE_RESISTANCE) {
            return resistanceColor[index];
        }
        else if (currentFaction == FACTION_CODE_ENLIGHTENED) {
            return enlightenedColor[index];
        }
        else {
            return 255;
        }
    }

    private int getCurrentPortalLevel() {
        return stringToInt(formPortalLevel.getText().toString());
    }

    private int getBrightness() {
        int brightness;
        int level = getCurrentPortalLevel();
        if (8 <= level) {
            brightness = 254;
        }
        else if (5 <= level) {
            brightness = 150;
        }
        else if (0 <= level) {
            brightness = 0;
        }
        else {
            brightness = 254;
        }
        return brightness;
    }

    private void updateFaction(int faction) {
        currentFaction = faction;
    }
    
    private void portalNeutralize() {
        initializeLightTimer();
        lightTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                lightHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        List<PHLight> allLights = getSortedLights();
                        PHLight light = allLights.get(currentLightIndex);
                        updateLightState(light, getLightRGBColor(light, 255, 0, 0), 254);
                        checkContinueAction(allLights.size(), NEUTRALIZE_STATE_RED_BRIGHT);
                    }
                });
            }
        }, Constants.TIMER_PERIOD, Constants.TIMER_DELAY);
    }

    private void updateLight() {
        initializeLightTimer();
        lightTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                lightHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        List<PHLight> allLights = getSortedLights();
                        PHLight light = allLights.get(currentLightIndex);
                        updateLightState(light, getLightRGBColor(light, getCurrentFactionColor(0),
                                getCurrentFactionColor(1), getCurrentFactionColor(2)), getBrightness());
                        checkContinueAction(allLights.size(), 0);
                    }
                });
            }
        }, Constants.TIMER_PERIOD, Constants.TIMER_DELAY);
    }

    private void checkContinueAction(int lightSize, int currentState) {
        if (currentLightIndex + 1 < lightSize) {
            currentLightIndex++;
        }
        else {
            lightTimer.cancel();
            currentLightIndex = 0;
            if (currentState == NEUTRALIZE_STATE_RED_BRIGHT) {
                // next change white color
                neutralizeLight(255, 0, 0, 0, NEUTRALIZE_STATE_RED_DARK, 2000);
            }
//            else if (currentState == NEUTRALIZE_STATE_WHITE_BRIGHT) {
//                // next change dark red color
//                neutralizeLight(255, 255, 255, 254, NEUTRALIZE_STATE_WHITE_BRIGHT, 1000);
//            }
            else if (currentState == NEUTRALIZE_STATE_RED_DARK) {
                // next change bright white color
                neutralizeLight(255, 255, 255, 254, NEUTRALIZE_STATE_WHITE_DEFAULT, 1000);
            }
        }
    }

    private void neutralizeLight(final int r, final int g, final int b, final int bright,
                                 final int state, int delay) {
        initializeLightTimer();
        lightTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                lightHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        List<PHLight> allLights = getSortedLights();
                        PHLight light = allLights.get(currentLightIndex);
                        updateLightState(light, getLightRGBColor(light, r, g, b), bright);
                        checkContinueAction(allLights.size(), state);
                    }
                });
            }
        }, delay, Constants.TIMER_PERIOD);
    }

    private void requestServer() {
        //queue
        RequestQueue queue = Volley.newRequestQueue(this);
        //url
        final String url = "https://betaspike.appspot.com";
        //Request
        StringRequest postRequest = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.v("","RESPONSE=" + s);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        //error
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String,String>();
                params.put("params","[[d71de0bc6b0345289118dbce675f850d.16]]");
                return params;
            }
        };
        queue.add(postRequest);
    }

}
