package jp.itnav.ingressportalfactionhue;

public class Constants {
    public static final int[][] COLOR_PATTERNS = {
            {255, 182, 208},
            {250, 224, 47},
            {0, 160, 255},
            {121, 252, 86},
            {194, 141, 253}
    };

    public static final int[] SOUND_RESOURCES = {
    };

    public static final int MAX_RGB=255;
    public static final int COLOR_PATTERN_RED = 0;
    public static final int COLOR_PATTERN_GREEN = 1;
    public static final int COLOR_PATTERN_BLUE = 2;
    public static final int LIGHT_MAX_BRIGHTNESS = 254;
    public static final int LIGHT_MIN_BRIGHTNESS = 0;

    public static final int TIMER_PERIOD = 150;
    public static final int TIMER_DELAY = 10;
    public static final int TIMER_DELAY_DARKEN = 5000;
}
